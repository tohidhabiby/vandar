<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\WalletController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware(['transaction'])
    ->post('wallets/{wallet}/transaction', [WalletController::class, 'transaction'])
    ->name('transaction');
Route::resource('users', UserController::class)->except(['update', 'destroy']);
Route::resource('wallets', WalletController::class)->only(['store', 'show', 'index']);
