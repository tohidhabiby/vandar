<?php

namespace App\Http\Resources\Wallet;

use App\Models\Accounting\Transaction;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array
    {
        return [
            Transaction::ID => $this->getId(),
            Transaction::IS_DEPOSIT => $this->getIsDeposit(),
            Transaction::AMOUNT => $this->getAmount(),
            Transaction::BEFORE_AMOUNT => $this->getBeforeAmount(),
            Transaction::AFTER_AMOUNT => $this->getAfterAmount(),
            Transaction::CREATED_AT => $this->getCreatedAt(),
        ];
    }
}
