<?php

namespace App\Http\Requests\Wallet;

use App\Models\Accounting\Transaction;
use App\Models\Accounting\Wallet;
use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WalletRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            Wallet::USER_ID => [
                'required',
                'integer',
                Rule::exists(User::TABLE, User::ID),
            ],
            Wallet::NAME => ['required', 'string'],
        ];
    }
}
