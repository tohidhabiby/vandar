<?php

namespace App\Http\Requests\Wallet;

use App\Models\Accounting\Transaction;
use App\Rules\CheckWithdrawRule;
use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Transaction::IS_DEPOSIT => 'required|boolean',
            Transaction::AMOUNT => [
                'required',
                'integer',
                'min:0',
                new CheckWithdrawRule($this->wallet, $this->get(Transaction::IS_DEPOSIT)),
            ],
        ];
    }
}
