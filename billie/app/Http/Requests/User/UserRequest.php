<?php

namespace App\Http\Requests\User;

use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            User::EMAIL => ['required', 'email', Rule::unique(User::TABLE, User::EMAIL)],
            User::NAME => ['required', 'string'],
            User::PASSWORD => ['required', 'string'],
        ];
    }
}
