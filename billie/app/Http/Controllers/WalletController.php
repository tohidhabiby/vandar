<?php

namespace App\Http\Controllers;

use App\Filters\WalletFilter;
use App\Http\Requests\Wallet\TransactionRequest;
use App\Http\Requests\Wallet\WalletRequest;
use App\Http\Resources\Wallet\WalletResource;
use App\Models\Accounting\Transaction;
use App\Models\Accounting\Wallet;
use App\Models\User\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Log;

class WalletController extends Controller
{
    /**
     * @param WalletFilter $filters
     * @return AnonymousResourceCollection
     */
    public function index(WalletFilter $filters): AnonymousResourceCollection
    {
        return WalletResource::collection(Wallet::filter($filters)->paginate());
    }

    /**
     * @param Wallet $wallet
     * @return WalletResource
     */
    public function show(Wallet $wallet): WalletResource
    {
        return new WalletResource($wallet->load('transactions'));
    }

    /**
     * @param WalletRequest $request
     * @return WalletResource
     */
    public function store(WalletRequest $request): WalletResource
    {
        $user = User::find($request->get(Wallet::USER_ID));

        return new WalletResource(Wallet::createObject($user, $request->get(Wallet::NAME)));
    }

    /**
     * @param TransactionRequest $request
     * @param Wallet $wallet
     * @return WalletResource
     * @throws AuthorizationException
     */
    public function transaction(TransactionRequest $request, Wallet $wallet): WalletResource
    {
        // if you use authentication you should check this wallet is this user wallet
//        $this->authorize('update');

        return new WalletResource(
            $wallet->doTransaction(
                $request->get(Transaction::AMOUNT),
                $request->get(Transaction::IS_DEPOSIT)
            )
        );
    }
}
