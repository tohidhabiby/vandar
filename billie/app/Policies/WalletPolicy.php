<?php

namespace App\Policies;

use App\Models\Accounting\Wallet;
use App\Models\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class WalletPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the model.
     *
     * @param User  $user
     * @param Wallet $wallet
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Wallet $wallet): bool
    {
        return $user->is($wallet->user);
    }
}
