<?php

namespace App\Models\User;

use App\Models\Accounting\Wallet;
use Habibi\Interfaces\FiltersInterface;
use Habibi\Traits\HasIdTrait;
use Habibi\Traits\HasNameTrait;
use Habibi\Traits\MagicMethodsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasIdTrait;
    use MagicMethodsTrait;
    use HasNameTrait;

    const TABLE = 'users';
    const ID = 'id';
    const NAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        self::NAME,
        self::EMAIL,
        self::PASSWORD,
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        self::PASSWORD,
    ];

    /**
     * Filter scope.
     *
     * @param Builder          $builder Builder.
     * @param FiltersInterface $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, FiltersInterface $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * @return HasMany
     */
    public function wallets(): HasMany
    {
        return $this->hasMany(Wallet::class);
    }

    /**
     * @param string $email
     * @param string $name
     * @param string $password
     * @return static
     */
    public static function createObject(string $email, string $name, string $password): self
    {
        $user = new static();
        $user->setEmail($email)->setName($name)->setPassword(bcrypt($password))->save();

        return $user;
    }
}
