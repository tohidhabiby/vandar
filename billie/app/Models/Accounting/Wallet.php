<?php

namespace App\Models\Accounting;

use App\Models\User\User;
use Habibi\Models\BaseModel;
use Habibi\Traits\HasAmountTrait;
use Habibi\Traits\HasNameTrait;
use Habibi\Traits\HasUserIdTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Cache;

class Wallet extends BaseModel
{
    use HasNameTrait;
    use HasUserIdTrait;
    use HasAmountTrait;

    const TABLE = 'wallets';
    const NAME = 'name';
    const AMOUNT = 'amount';
    const USER_ID = 'user_id';

    private int $amount;

    /**
     * @param User $user User.
     * @param string $name Name.
     *
     * @return $this
     */
    public static function createObject(User $user, string $name): self
    {
       $wallet = new static();
       $wallet->setUserId($user->getId())->setName($name)->save();

       return $wallet;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        if (isset($this->amount) && $this->amount) {
            return $this->amount;
        }

        return $this->amount = (int)$this->transactions()->sum(Transaction::AMOUNT);
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @param int $amount
     * @param bool $isDeposit
     * @return $this
     */
    public function doTransaction(int $amount, bool $isDeposit): self
    {
        $before = $this->getAmount();
        $amount = $isDeposit ? $amount : -$amount;
        $this->transactions()->create(
            [
                Transaction::AMOUNT => $amount,
                Transaction::BEFORE_AMOUNT => $before,
                Transaction::AFTER_AMOUNT => $before + $amount,
                Transaction::IS_DEPOSIT => $isDeposit,
            ]
        );
        $this->setAmount($before + $amount);
        // it is not very good for this action here, these lines should be in repository or in a job not here
        // but I have to done this task just in 3 days, sorry
        $key = 'wallet_' . $this->getId();
        $array = Cache::get($key);
        unset($array[key($array)]);
        Cache::put($key, $array);

        return $this;
    }

    /**
     * @return HasMany
     */
    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }
}
