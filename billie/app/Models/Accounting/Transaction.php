<?php

namespace App\Models\Accounting;

use Habibi\Models\BaseModel;
use Habibi\Traits\HasAmountTrait;
use Habibi\Traits\HasWalletIdTrait;

class Transaction extends BaseModel
{
    use HasAmountTrait;
    use HasWalletIdTrait;

    const TABLE = 'transactions';
    const WALLET_ID = 'wallet_id';
    const AMOUNT = 'amount';
    const BEFORE_AMOUNT = 'before_amount';
    const AFTER_AMOUNT = 'after_amount';
    const IS_DEPOSIT = 'is_deposit';

    protected $fillable = [
        self::AFTER_AMOUNT,
        self::BEFORE_AMOUNT,
        self::AMOUNT,
        self::WALLET_ID,
        self::IS_DEPOSIT,
    ];
}
