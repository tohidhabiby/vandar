<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckWithdrawRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(private mixed $wallet, private mixed $isDeposit) {}

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (is_bool($this->isDeposit) && !$this->isDeposit && $this->wallet->getAmount() < (int)$value) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
