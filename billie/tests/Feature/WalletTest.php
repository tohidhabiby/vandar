<?php

namespace Tests\Feature;

use App\Models\Accounting\Transaction;
use App\Models\Accounting\Wallet;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class WalletTest extends TestCase
{
    use WithFaker;
    /**
     * @test
     */
    public function userCanCreateWallet()
    {
        $user = User::factory()->create();
        $name = $this->faker->word;
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::USER_ID => $user->getId(),
                Wallet::NAME => $name,
            ]
        )->assertCreated();
        $this->assertEquals($response->getOriginalContent()->getUserId(), $user->getId());
        $this->assertEquals($response->getOriginalContent()->getName(), $name);
    }

    /**
     * @test
     */
    public function userCanGetAWallet()
    {
        $wallet = Wallet::factory()->create();
        $response = $this->getJson(route('wallets.show', $wallet))->assertOk();
        $this->assertTrue($response->getOriginalContent()->is($wallet));
    }

    /**
     * @test
     */
    public function userCanDoTransactions()
    {
        $wallet = Wallet::factory()->create();
        $amount = $this->faker->numberBetween(1, 1000000000);
        $response = $this->postJson(
            route('transaction', $wallet),
            [
                Transaction::IS_DEPOSIT => true,
                Transaction::AMOUNT => $amount,
            ]
        );
        $this->assertEquals($response->getOriginalContent()->getAmount(), $amount);
        $transaction = $wallet->transactions()->latest()->first();
        $this->assertEquals($transaction->getAmount(), $amount);
        $this->assertEquals($transaction->getBeforeAmount(), 0);
        $this->assertEquals($transaction->getAfterAmount(), $amount);
        $response = $this->postJson(
            route('transaction', $wallet),
            [
                Transaction::IS_DEPOSIT => false,
                Transaction::AMOUNT => $amount,
            ]
        );
        $this->assertEquals($response->getOriginalContent()->getAmount(), 0);
        $transaction = $wallet->transactions()->latest()->first();
        $this->assertEquals($transaction->getAmount(), -$amount);
        $this->assertEquals($transaction->getBeforeAmount(), $amount);
        $this->assertEquals($transaction->getAfterAmount(), 0);
    }

    /**
     * @test
     */
    public function userCanNotWithdrawMoreThanRestAmount()
    {
        $transaction = Transaction::factory()->create([Transaction::IS_DEPOSIT => true]);
        $response = $this->postJson(
            route('transaction', $transaction->wallet),
            [
                Transaction::IS_DEPOSIT => false,
                Transaction::AMOUNT => $transaction->getAfterAmount() + 1,
            ]
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->json()['errors'];
        $this->assertArrayHasKey(Transaction::AMOUNT, $content);
    }

    /**
     * @return void
     */
    public function userCanNotCreateWalletWithWrongData()
    {
        $response = $this->postJson(route('wallets.store'), []);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->getOriginalContent()->toArray();
        $this->assertArrayHasKey(Wallet::NAME, $content);
        $this->assertArrayHasKey(Wallet::USER_ID, $content);
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::USER_ID => $this->faker->numberBetween(100, 1000),
                Wallet::NAME => true
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->getOriginalContent()->toArray();
        $this->assertArrayHasKey(Wallet::NAME, $content);
        $this->assertArrayHasKey(Wallet::USER_ID, $content);
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::USER_ID => 'test',
                Wallet::NAME => 123
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->getOriginalContent()->toArray();
        $this->assertArrayHasKey(Wallet::NAME, $content);
        $this->assertArrayHasKey(Wallet::USER_ID, $content);
    }
}
