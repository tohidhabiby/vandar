<?php

namespace Database\Factories\Accounting;

use App\Models\Accounting\Wallet;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class WalletFactory extends Factory
{
    protected $model = Wallet::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            Wallet::NAME => $this->faker->word(),
            Wallet::USER_ID => User::factory(),
        ];
    }
}
